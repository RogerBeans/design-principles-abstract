﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicShopDiversified.MusicShop
{
    public class Guitar
    {
        public Guitar(string serialNumber, double price, GuitarSpec guitarSpec)
        {

            this.SerialNumber = serialNumber;
            this.Price = price;
            this._guitarSpec = guitarSpec;


        }

        public string SerialNumber { get; set; }
        public double Price { get; set; }

        private GuitarSpec _guitarSpec;

        public GuitarSpec getSpec()
        {
            return _guitarSpec;
        }
    }
    }
