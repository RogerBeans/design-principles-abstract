﻿using System;
using System.Dynamic;

namespace MusicShopDiversified.MusicShop
{
    public class MandolinSpec : InstrumentSpec
    {
        private string _style;
        public MandolinSpec(Builder builder, string model, Type type, int numOfStrings, Wood backWood, Wood topWood, string style) : base(builder, model, type, backWood, topWood)
        {
            _style = style;
        }

        public string Style { get; set; }

        public bool Matches(MandolinSpec otherSpec)
        {

            if (Builder != otherSpec.Builder)
                return false;

            if (String.IsNullOrEmpty(Model) && (!Model.Equals(otherSpec.Model)))
                return false;

            if (Type != otherSpec.Type)
                return false;

            if (BackWood != otherSpec.BackWood)
                return false;

            if (TopWood != otherSpec.TopWood)
                return false;

            if (Style != otherSpec.Style)
                return false;

            return true;

        }
    }
}