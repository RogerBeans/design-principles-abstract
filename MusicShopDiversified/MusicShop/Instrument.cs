﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicShopDiversified.MusicShop
{
    public abstract class Instrument
    {
        private string _serialNumber;
        private double _price;
        private InstrumentSpec _spec;

        public Instrument(string serialNumber, double price, InstrumentSpec spec) {
            this._serialNumber = serialNumber;
            this._price = price;
            this._spec = spec;
        }

        // get and set methods for serial number and price
        public InstrumentSpec getSpec() {
            return _spec;
        }
    }
}
