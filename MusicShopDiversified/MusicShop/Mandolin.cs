﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicShopDiversified.MusicShop
{
    public class Mandolin
    {
        private MandolinSpec _mandolinSpec;
        public Mandolin(string serialNumber, double price, MandolinSpec MandolinSpec)
        {

            this.SerialNumber = serialNumber;
            this.Price = price;
            this._mandolinSpec = MandolinSpec;


        }

        public string SerialNumber { get; set; }
        public double Price { get; set; }



        public MandolinSpec getSpec()
        {
            return _mandolinSpec;
        }
    }
    }
