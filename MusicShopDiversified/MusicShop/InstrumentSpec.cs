﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicShopDiversified.MusicShop
{
    public abstract class InstrumentSpec
    {
        private Builder builder;
        private string model;
        private Type type;
        private Wood backWood;
        private Wood topWood;

        public InstrumentSpec(Builder builder, string model, Type type, Wood backWood, Wood topWood) {
            this.builder = builder;
            this.model = model;
            this.type = type;
            this.backWood = backWood;
            this.topWood = topWood;
        }

        public Builder Builder { get; set; }
        public string Model { get; set; }
        public Type Type { get; set; }
        public Wood BackWood { get; set; }
        public Wood TopWood { get; set; }

        public bool Matches(InstrumentSpec otherSpec)
        {

            if (Builder != otherSpec.Builder)
                return false;

            if (String.IsNullOrEmpty(Model) && (!Model.Equals(otherSpec.Model)))
                return false;

            if (Type != otherSpec.Type)
                return false;

            if (BackWood != otherSpec.BackWood)
                return false;

            if (TopWood != otherSpec.TopWood)
                return false;

            return true;

        }

    }
}
