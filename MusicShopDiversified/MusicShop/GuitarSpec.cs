﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicShopDiversified.MusicShop
{
    public class GuitarSpec : InstrumentSpec
    {

        private int _numOfStrings;

        public GuitarSpec(Builder builder, string model, Type type, int numOfStrings, Wood backWood, Wood topWood) : base(builder, model, type, backWood, topWood)
        {
            _numOfStrings = numOfStrings;
        }

        public int  NumOfStrings { get; set; }
        public bool Matches(GuitarSpec otherSpec) {

            if (Builder != otherSpec.Builder)
                return false;

            if (String.IsNullOrEmpty(Model) && (!Model.Equals(otherSpec.Model)))
                return false;

            if (Type != otherSpec.Type)
                return false;

            if (BackWood != otherSpec.BackWood)
                return false;

            if (TopWood != otherSpec.TopWood)
                return false;

            if (NumOfStrings != otherSpec.NumOfStrings)
                return false;

            return true;
        }



    }
}
