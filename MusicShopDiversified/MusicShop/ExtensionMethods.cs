﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicShopDiversified.MusicShop
{
   public class ExtensionMethods
    {

    }

    public enum Type
    {
        ACOUSTIC, ELECTRIC
    }

    public enum Builder
    {
        FENDER, MARTIN, GIBSON, COLLINGS, OLSON, RYAN, PRS
    }

    public enum Wood
    {
        INDIAN_ROSEWOOD, MAHOGANY, MAPLE, CEDAR, ALDER, SITKA, ADIRONDACK
    }

    public enum NoOfStrings
    { 
        Six = 6,
        Tweleve = 12,
    }
}
